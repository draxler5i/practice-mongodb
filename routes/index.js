const express = require('express');
const router = express.Router();

router.use('/task', require('./task.router'));

module.exports = router;
