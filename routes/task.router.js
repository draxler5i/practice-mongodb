const express = require('express');
const router = express.Router();
const Task = require('../model/task');

router.get('/', async (req, res) => {
  try {
    const tasks = await Task.find();
    res.status(200).json(tasks);
  } catch (err) {
    console.log('Error on get allTasks ', err);
    res.status(500).json({
      message: 'An error ocurred on  server',
      error: err
    });
  }
});

router.post('/', async (req, res) => {
  try {
    const task = new Task(req.body);
    await task.save();
    res.status(201).json(task);
  } catch (err) {
    console.log('Error on create task ', err);
    res.status(500).json({
      message: 'An error ocurred on  server',
      error: err
    });
  }
});

router.get('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const task = await Task.findById(id);
    res.status(200).json(task);
  } catch (err) {
    console.log('Error on get taskById ', err);
    res.status(500).json({
      message: 'An error ocurred on  server',
      error: err
    });
  }
});


router.put('/:id', async (req, res) => {
  try {
    const task = await Task.findByIdAndUpdate(
      req.params.id,
      req.body,
      { new: false }
    );
    res.status(200).json(task);
  } catch (err) {
    console.log('Error on update allTasks ', err);
    res.status(500).json({
      message: 'An error ocurred on  server',
      error: err
    });
  }
});

router.delete("/:id", async (req, res) => {
  try {
    const taskRemove = await Task.findByIdAndRemove(req.params.id);
    res.status(200).json(taskRemove);
  } catch (err) {
    console.log('Error on delete allTasks ', err);
    res.status(500).json({
      message: 'An error ocurred on  server',
      error: err
    });
  }
});

module.exports = router;
